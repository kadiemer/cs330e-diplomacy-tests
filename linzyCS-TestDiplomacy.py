#!/usr/bin/env python3

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import dict_add
from Diplomacy import diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):

    # -----
    # solve
    # -----
    def test_dict_add_1(self):
        d1={'A':['C'],'D':['E','F']}
        dict_add(d1,'B','G')
        self.assertEqual(d1,{'A':['C'],'B':['G'],'D':['E','F']})
        
    def test_dict_add_2(self):
        d2={'A':['C'],'D':['E','F']}
        dict_add(d2,'D','G')
        self.assertEqual(d2,{'A':['C'],'D':['E','F','G']})
    def test_dict_add_3(self):
        d3={'A':['C'],'D':['E','F']}
        dict_add(d3,'A','G')
        self.assertEqual(d3,{'A':['C','G'],'D':['E','F']})

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

    def test_solve_2(self):
        r=StringIO( "A PearlHarbor Hold\nB Tokyo Move PearlHarbor\nC Osaka Move PearlHarbor\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

    def test_solve_3(self):
        r =StringIO( "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Barcelona\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC Barcelona\n")




# ----
# main
# ----


if __name__ == "__main__":
    main()

