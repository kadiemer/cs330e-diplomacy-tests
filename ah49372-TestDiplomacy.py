
# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# Testdiplomacy
# -----------


class Testdiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "A Madrid Hold\n"
        k = diplomacy_read(s)
        self.assertEqual(k, ["A", "Madrid", "Hold"])

    def test_read_2(self):
        s = "B Barcelona Move Madrid\n"
        k = diplomacy_read(s)
        self.assertEqual(k, ["B", "Barcelona", "Move", "Madrid"])

    def test_read_3(self):
        s = "C Paris Support B\n"
        k = diplomacy_read(s)
        self.assertEqual(k, ["C", "Paris", "Support", "B"])

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = diplomacy_eval("A Madrid Move Barcelona\n B Barcelona Hold\n C London Support B\n D Austin Move London\n")
        self.assertEqual(v, "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_eval_2(self):
        v = diplomacy_eval("A Madrid Support B\nB Barcelona Move London\nC London Hold\n")
        self.assertEqual(v, "A Madrid\nB London\nC [dead]\n")

    def test_eval_3(self):
        v = diplomacy_eval("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\nF NewYork Move Austin\n")
        self.assertEqual(v, "A [dead]\nB Madrid\nC [dead]\nD Paris\nE [dead]\nF [dead]\n")

    def test_eval_4(self):
        v = diplomacy_eval("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London")
        self.assertEqual(v, "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, "A [dead]\nB Madrid\nC [dead]\nD Paris\nE [dead]\nF [dead]\n")
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\nE [dead]\nF [dead]\n")

    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, "A Madrid\nB London\nC [dead]\n")
        self.assertEqual(w.getvalue(), "A Madrid\nB London\nC [dead]\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Support B\nB Barcelona Move London\nC London Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB London\nC [dead]\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")
            
    def test_solve_4(self):
        r = StringIO("A Madrid Hold\nB London Move Madrid\nC Austin Move London")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC London\n")

# ----
# main
# ----


if __name__ == "__main__":
    main()

'''
#pragma: no cover
'''